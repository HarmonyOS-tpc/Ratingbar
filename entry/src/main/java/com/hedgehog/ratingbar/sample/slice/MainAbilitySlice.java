/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.hedgehog.ratingbar.sample.slice;

import com.hedgehog.ratingbar.RatingBar;
import com.hedgehog.ratingbar.ResourceTable;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.window.dialog.ToastDialog;

/**
 * MainAbilitySlice
 */
public class MainAbilitySlice extends AbilitySlice {
    private static final float PADDING_100 = 10;

    private static final int STAR_COUNT = 10;

    private static final float IMAGE_WIDTH = 70f;

    private static final float IMAGE_HEIGHT = 90f;

    private static final float DEFAULT_STAR_VALUE = 5f;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        readyRatingBar();
    }

    private void readyRatingBar() {
        RatingBar mRatingBar = null;
        if (findComponentById(ResourceTable.Id_ratingbar) instanceof RatingBar) {
            mRatingBar = (RatingBar) findComponentById(ResourceTable.Id_ratingbar);
            mRatingBar.setStarEmptyDrawable(ResourceTable.Media_star_empty);
            mRatingBar.setStarHalfDrawable(ResourceTable.Media_star_half);
            mRatingBar.setStarFillDrawable(ResourceTable.Media_star_full);
            mRatingBar.setStarCount(STAR_COUNT);
            mRatingBar.setStar(DEFAULT_STAR_VALUE);

            mRatingBar.halfStar(true);
            mRatingBar.setmClickable(true);
            mRatingBar.setStarImageWidth(IMAGE_WIDTH);
            mRatingBar.setStarImageHeight(IMAGE_HEIGHT);
            mRatingBar.setImagePadding(PADDING_100);
            mRatingBar.setOnRatingChangeListener(RatingCount -> {
                new ToastDialog(MainAbilitySlice.this).setText("The fill star is " + RatingCount).show();
            });
        }
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
