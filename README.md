# RatingBar

Developers can use RatingBar to implement show rating's, like feedback or other needed.

# RatingBar includes :
* Provides A custom RatingBar in openharmony

# Usage Instructions

 1. In xml
        <?xml version="1.0" encoding="utf-8"?>
        <DirectionalLayout
            xmlns:ohos="http://schemas.huawei.com/res/ohos"
            xmlns:app="http://schemas.huawei.com/hap/res-auto"
            ohos:height="match_parent"
            ohos:width="match_parent"
            ohos:orientation="vertical"
            ohos:padding="20vp">

            <com.hedgehog.ratingbar.RatingBar
                ohos:id="$+id:ratingbar"
                ohos:height="match_content"
                ohos:width="match_parent"
                ohos:alignment="center"
                ohos:layout_alignment="horizontal_center"
                app:clickable="true"
                app:halfstart="true"
                app:starImageHeight="90"
                app:starImagePadding="20"
                app:starImageWidth="70"
                />

        </DirectionalLayout>

###method statement

| method|    action |
| :-------- | :--------|
|app:clickable="true"   |Could you click|
|app:halfstart="true"|Whether to support half a star|
|app:starCount="N"|The total number of stars （int）|
|app:starImageHeight="90"|the height of the stars|
|app:starImageWidth="70"|the width of the star|
|app:starImagePadding="20"|the padding of the star|

#2. In Java code

      RatingBar mRatingBar = (RatingBar) findComponentById(ResourceTable.Id_ratingbar);
            mRatingBar.setStarEmptyDrawable(ResourceTable.Media_star_empty);
            mRatingBar.setStarHalfDrawable(ResourceTable.Media_star_half);
            mRatingBar.setStarFillDrawable(ResourceTable.Media_star_full);
            mRatingBar.setStarCount(STAR_COUNT);
            mRatingBar.halfStar(true);
            mRatingBar.setmClickable(true);
            mRatingBar.setStarImageWidth(IMAGE_WIDTH);
            mRatingBar.setStarImageHeight(IMAGE_HEIGHT);
            mRatingBar.setStar(DEFAULT_STAR_VALUE);
            mRatingBar.setImagePadding(PADDING_100);
            mRatingBar.setOnRatingChangeListener(RatingCount -> {
                new ToastDialog(MainAbilitySlice.this).setText("the fill star is" + RatingCount).show();
            });

###method statement

| method      |    action |
| :-------- | :--------:|
|mRatingBar.setStarCount(5);|The total number of stars|
|mRatingBar.setStar(2.5f);|Fill the total number of the stars|
|mRatingBar.halfStar(true);|Whether set up half a star|
|mRatingBar.setmClickable(true);|Whether the Settings can be click|
|mRatingBar.setStarImageWidth(120f);|the width of the stars|
|mRatingBar.setStarImageHeight(60f);|the height of the stars|
|mRatingBar.setImagePadding(35);|the padding of the stars|

# Installation tutorial

1. Using the ratingbar.har, make sure to add ratingbar.har file in the entry/libs folder.

        Modify entry build.gradle as below :

           dependencies {
                implementation fileTree(dir: 'libs', include: [' *.jar', ' *.har'])
           }


2.  For using RatingBar module in sample app, Add the dependencies in entry/build.gradle as below :

        Modify entry build.gradle as below :

           dependencies {
               implementation project(path: ':ratingbar')
           }


3. For using RatingBar from a remote repository in separate application, add the below dependencies :

    	Modify entry build.gradle as below :

    	dependencies {
    	    implementation 'io.openharmony.tpc.thirdlib:Ratingbar:1.0.1'
    	}



# License

    Copyright 2015 hedge_hog

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.